import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from './List'

class KanbanBoard extends Component {
  render() {
    return(
      <div className="app">
        <List
          title="To Do"
          cards={
            this.props.cards.filter((card) => card.status === 'todo')
          }
        />
        <List
          title="In Progress"
          cards={
            this.props.cards.filter((card) => card.status === 'in-progress')
          }
        />
        <List
          title="Done"
          cards={
            this.props.cards.filter((card) => card.status === 'done')
          }
        />
      </div>
    )
  }
}

KanbanBoard.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default KanbanBoard
