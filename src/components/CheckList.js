import React, {Component} from 'react'

class CheckList extends Component {
  render() {
    const tasks = this.props.tasks.map((task) => (
      <li key={task.id} className="checkList_task">
        <label>
          <input type="checkbox" defaultChecked={task.done} className="checkBox" />
          <span className="checkList_task--remove">{task.name}</span>
        </label>
      </li>
    ))
    const sideColor = {
      backgroundColor: this.props.color,
    }
    return(
      <div className="tasks">
        <ul className="taskList" style={sideColor}>{tasks}</ul>
        <input
          type="text"
          className="checkList--addtask"
          placeholder="Ttpe then hit Enter to add task"
        />
      </div>
    )
  }
}

export default CheckList
