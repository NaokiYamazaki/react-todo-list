import React, { Component } from 'react'
import CheckList from './CheckList'
import Marked from 'marked'

class Card extends Component {
  constructor(...args) {
    super(...args)
    this.state = {
      showDetails: false,
    }
    this.toggleDetails = this.toggleDetails.bind(this)
  }
  toggleDetails() {
    this.setState({
      showDetails: !this.state.showDetails
    })
  }
  render() {
    let cardDetails
    if(this.state.showDetails) {
      cardDetails = (
        <div className="card_details">
          {this.props.description}
          <span dangerouslySetInnerHTML={{__html: Marked(this.props.description)}} />
          <CheckList cardId={this.props.id} tasks={this.props.tasks} color={this.props.color} />
        </div>
      )
    }
    return(
      <div className="card">
        <div className={this.state.showDetails ? 'card_title card_title--is-open' : 'card_title'}
          onClick={this.toggleDetails}
          role="presentation"
        >
          {this.props.title}
        </div>
        {cardDetails}
      </div>
    )
  }
}

export default Card
