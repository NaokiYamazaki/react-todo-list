import React from 'react'
import ReactDOM from 'react-dom'
import KanbanBoard from './components/KanbanBoard'
import registerServiceWorker from './registerServiceWorker'
import './index.css'

const cardList = [
  {
    id: 1,
    title: 'Read the Book',
    description: 'I should read the **whole** book',
    color: '#dff1d6',
    status: 'in-progress',
    tasks:[
      {
        id: 1,
        name: 'Contactlist Example',
        done: true,
      },
      {
        id: 2,
        name: 'Kanban Example',
        done: false,
      },
      {
        id: 3,
        name: 'My own experiments',
        done: false,
      },
    ],
  },
  {
    id: 1,
    title: 'Write some code',
    description: 'Code along with the samples in the book. The complete can be found at [github](https://github.com/pro-react)',
    color: '#d6f1ef',
    status: 'todo',
    tasks: [
      {
        id: 1,
        name: 'Contactlist Example',
        done: true,
      },
      {
        id: 2,
        name: 'Kanban Example',
        done: false,
      },
      {
        id: 3,
        name: 'My own experiments',
        done: false,
      },
    ],
  },
]

ReactDOM.render(
  <KanbanBoard cards={cardList} />,
  document.getElementById('root')
)
registerServiceWorker()
